#!/usr/bin/env python

from netmiko import Netmiko
from getpass import getpass

dlink = {
    'host': '10.90.90.90',
    'username': 'admin',
    'password': getpass(),
    'device_type': 'cisco_ios',
}

cfg_file = 'config_changes.txt'
net_connect = Netmiko(**dlink)

print()
print(net_connect.find_prompt())
output = net_connect.send_config_from_file(cfg_file)
print(output)
print()

net_connect.save_config()
net_connect.disconnect()