#!/usr/bin/env python
from netmiko import Netmiko
from getpass import getpass

net_connect = Netmiko(
    "10.90.90.90",
    username="admin",
    password="admin",
    device_type="cisco_ios",
)

print(net_connect.find_prompt())
net_connect.disconnect()
