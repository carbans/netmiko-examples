#!/usr/bin/env python
from netmiko import Netmiko
from getpass import getpass

dlink = {
    'host': '10.90.90.90',
    'username': 'admin',
    'password': 'admin',
    'device_type': 'cisco_ios',
}

net_connect = Netmiko(**dlink)
command = 'show ip int brief'

print()
print(net_connect.find_prompt())
output = net_connect.send_command(command)
net_connect.disconnect()
print(output)
print()
